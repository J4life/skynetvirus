package fr.ing.skynetvirus;

import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Game input constraints : 2 ≤ N ≤ 500 1 ≤ L ≤ 1000 1 ≤ E ≤ 20 0 ≤ N1, N2 < N 0
 * ≤ SI < N 0 ≤ C1, C2 < N
 */
@ContextConfiguration(classes = ApplicationConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class SkynetServiceTestsSuite {

	@Autowired
	private SkynetVirusService sut;

	private InputStream inputStream;

	@Test
	public void should_raise_exception_when_nodes_count_is_less_then_two() throws Exception {
		try {
			inputStream = new ByteArrayInputStream("1".getBytes());
			System.setIn(inputStream);
			sut.doLaunch();
			fail("No exception thrown : ConstraintException should be raised because the input is wrong");
		} catch (InputValidationException exp) {
			if (exp.getError() != InputValidationException.InputValidationError.NODES_COUNT)
				fail("An unexpected exception has been raised");
		}

	}

	@Test
	public void should_raise_exception_when_nodes_count_is_greater_then_five_hundred() throws Exception {
		try {
			inputStream = new ByteArrayInputStream("501".getBytes());
			System.setIn(inputStream);
			sut.doLaunch();
			fail("No exception thrown : ConstraintException should be raised because the input is wrong");
		} catch (InputValidationException exp) {
			if (exp.getError() != InputValidationException.InputValidationError.NODES_COUNT)
				fail("An unexpected exception has been raised");
		}

	}

	@Test
	public void should_raise_exception_when_links_count_is_less_then_one() throws Exception {
		try {
			inputStream = new ByteArrayInputStream("2 0".getBytes());
			System.setIn(inputStream);
			sut.doLaunch();
			fail("No exception thrown : ConstraintException should be raised because the input is wrong");
		} catch (InputValidationException exp) {
			if (exp.getError() != InputValidationException.InputValidationError.LINKS_COUNT)
				fail("An unexpected exception has been raised");
		}
	}

	@Test
	public void should_raise_exception_when_links_count_is_greater_then_one_thousand() throws Exception {
		try {
			inputStream = new ByteArrayInputStream("3 1001".getBytes());
			System.setIn(inputStream);
			sut.doLaunch();
			fail("No exception thrown : ConstraintException should be raised because the input is wrong");
		} catch (InputValidationException exp) {
			if (exp.getError() != InputValidationException.InputValidationError.LINKS_COUNT)
				fail("An unexpected exception has been raised");
		}
	}

	@Test
	public void should_raise_exception_when_gateways_count_is_less_then_one() throws Exception {
		try {
			inputStream = new ByteArrayInputStream("2 1 0".getBytes());
			System.setIn(inputStream);
			sut.doLaunch();
			fail("No exception thrown : ConstraintException should be raised because the input is wrong");
		} catch (InputValidationException exp) {
			if (exp.getError() != InputValidationException.InputValidationError.GATEWAYS_COUNT)
				fail("An unexpected exception has been raised");
		}
	}

	@Test
	public void should_not_raise_exception_with_good_inputs() throws Exception {
		inputStream = new ByteArrayInputStream("3 2 1 1 2 1 0 1 2".getBytes());
		System.setIn(inputStream);
		List<Link> links = sut.doLaunch();
		Assertions.assertThat(links).isNotNull();
		Assertions.assertThat(links).hasSize(1);
		Assertions.assertThat(links).containsSequence(new Link(new Node(2), new Node(1)));
	}

}
