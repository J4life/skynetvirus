package fr.ing.skynetvirus;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "fr.ing.skynetvirus")
public class ApplicationConfiguration {

}
