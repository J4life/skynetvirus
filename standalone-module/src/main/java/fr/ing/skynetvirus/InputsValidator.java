package fr.ing.skynetvirus;

import org.springframework.stereotype.Component;

@Component
public class InputsValidator {
	
	public void validateSkynetAgentPosition(int skynetAgentPosition) throws InputValidationException {
		if(skynetAgentPosition <1 || skynetAgentPosition > 20) throw new InputValidationException(InputValidationException.InputValidationError.SKYNET_AGENT_POSITION); 
		
	}

	public void validateGatewaysCount(int gatewaysCount) throws InputValidationException {
		if(gatewaysCount<1 || gatewaysCount > 20) throw new InputValidationException(InputValidationException.InputValidationError.GATEWAYS_COUNT); 
		
	}

	public void validateLinksCount(int linksCount) throws InputValidationException {
		if(linksCount<1 || linksCount > 1000) throw new InputValidationException(InputValidationException.InputValidationError.LINKS_COUNT); 
		
	}

	public void validateNodesCount(int treeSize) throws InputValidationException {
		if(treeSize <2 || treeSize >500) throw new InputValidationException(InputValidationException.InputValidationError.NODES_COUNT); 
		
	}

}
