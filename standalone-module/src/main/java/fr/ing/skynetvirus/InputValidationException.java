package fr.ing.skynetvirus;

public class InputValidationException extends Exception{
	
	private static final long serialVersionUID = -5047512896325540875L;
	
	private InputValidationError error;
	
	enum InputValidationError
	{
		SKYNET_AGENT_POSITION("Spercified agent position is incorrect"),
		LINKS_COUNT("Links count must be between 1 and 1000"),
		GATEWAYS_COUNT("Gateways count must be between 1 and 20"),
		NODES_COUNT("Nodes count must be between 2 and 500");
		
		private String description;

		private InputValidationError(String description) {
			this.description = description;
		}
		
		public String getDescription()
		{
			return description;
		}
	}
	
	public InputValidationException(InputValidationError error) {
		this.error = error;
	}
	
	@Override
	public String getMessage() {
		return error.getDescription();
	}
	
	public InputValidationError getError()
	{
		return error;
	}

}
