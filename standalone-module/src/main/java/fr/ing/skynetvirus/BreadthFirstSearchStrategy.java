package fr.ing.skynetvirus;

import java.util.LinkedList;

import org.springframework.stereotype.Component;

@Component
public class BreadthFirstSearchStrategy implements ShortestPathStrategy {

	@Override
	public LinkedList<Node> findShortestPath(Tree tree, Node startNode, Node arrivalNode) {
		boolean[] isNodeExplored = new boolean[tree.getSize()];
		int[] parentNode = new int[tree.getSize()];
		LinkedList<Node> shortestPath = new LinkedList<>();

		shortestPath.add(startNode);
		isNodeExplored[startNode.getValue()] = true;
		Node lastVisitedNode = null;

		while ((shortestPath.size() != 0) && (lastVisitedNode = shortestPath.remove()) != arrivalNode) {
			for (Node m : lastVisitedNode.getNeighbours()) {
				if (!isNodeExplored[m.getValue()]) {
					isNodeExplored[m.getValue()] = true;
					parentNode[m.getValue()] = lastVisitedNode.getValue();
					shortestPath.add(m);
				}
			}
		}

		return reversePath(tree,startNode, arrivalNode, parentNode, lastVisitedNode);
		
	}

	private LinkedList<Node> reversePath(Tree tree, Node startNode, Node arrivalNode, int[] parentNode,
			 Node lastVisitedNode) {
		LinkedList<Node> reversedPath = new LinkedList<>();

		if (lastVisitedNode == arrivalNode) {
			while (lastVisitedNode != startNode) {
				reversedPath.addFirst(lastVisitedNode);
				lastVisitedNode = tree.getNodes()[parentNode[lastVisitedNode.getValue()]];
			}
			reversedPath.addFirst(lastVisitedNode);
		}
		return reversedPath;
	}

	
}
