package fr.ing.skynetvirus;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.assertj.core.util.Preconditions;

public final class Link {
	
	private final Node firstNode;
	private final Node secondNode;
	
	public Link(Node firstNode, Node secondNode) {
		super();
		this.firstNode = firstNode;
		this.secondNode = secondNode;
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

	public Node getFirstNode() {
		return firstNode;
	}

	public Node getSecondNode() {
		return secondNode;
	}
	
	@Override
	public boolean equals(Object obj) {
		Preconditions.checkNotNull(obj);
		Preconditions.checkNotNull(firstNode);
		Preconditions.checkNotNull(secondNode);
		Link link = (Link)obj;
		return firstNode.equals(link.getFirstNode()) && secondNode.equals(link.getSecondNode());
	}
	

}
