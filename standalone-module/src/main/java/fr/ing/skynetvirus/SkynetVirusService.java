package fr.ing.skynetvirus;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SkynetVirusService {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(SkynetVirusService.class);

	@Autowired
	private InputsValidator inputsValidator;
	
	@Autowired
	private ShortestPathStrategy shortestPathAlgorithm;
	
	private Scanner inputScanner;
	
	private Tree tree;
	

	private void init() throws InputValidationException
	{
		inputScanner = new Scanner(System.in);
		tree = readInputData();
	}
	
	public List<Link> doLaunch() throws InputValidationException {
		int skynetAgentPosition;
		List<Link> cutedLinks = new ArrayList<>();

		init();
		try
		{
			while (true) {
				skynetAgentPosition = inputScanner.nextInt();
				inputsValidator.validateSkynetAgentPosition(skynetAgentPosition);
				cutedLinks.add(playAnotherTour(tree, skynetAgentPosition));
			}
		}
		catch(NoSuchElementException exp){
			LOGGER.info("SkyAgnet stopped moving, so teminate this pool execution.");
		}
		
		return cutedLinks;
	}

	private Link playAnotherTour(Tree tree, int skynetAgentPosition) {
		LinkedList<Node> nextPathToUnlink = findNextPathToUnlink(skynetAgentPosition);
		return unlink(nextPathToUnlink);
	}

	private LinkedList<Node> findNextPathToUnlink(int skynetAgentPosition) {
		int minLength = Integer.MAX_VALUE;
		LinkedList<Node> nextPathToUnlink = null;
		for (int i = 0; i < tree.getSize(); i++) {
			if (tree.getNode(i).isGateway()) {
				LinkedList<Node> shortestPath = shortestPathAlgorithm.findShortestPath(tree, tree.getNode(skynetAgentPosition), tree.getNode(i));
				int length = shortestPath.size();
				if ((length != 0) && (length < minLength)) {
					minLength = length;
					nextPathToUnlink = shortestPath;
				}
			}
		}
		return nextPathToUnlink;
	}

	private Tree readInputData() throws InputValidationException {
		int treeSize = inputScanner.nextInt();
		inputsValidator.validateNodesCount(treeSize);

		int linksCount = inputScanner.nextInt();
		inputsValidator.validateLinksCount(linksCount);

		int gatewaysCount = inputScanner.nextInt();
		inputsValidator.validateGatewaysCount(gatewaysCount);

		tree = new Tree(treeSize, gatewaysCount);

		linkNodes(linksCount, tree);
		defineGateways(gatewaysCount);

		return tree;
	}

	private void defineGateways(int gatewaysCount) {
		for (int i = 0; i < gatewaysCount; i++) {
			tree.getNode(inputScanner.nextInt()).setGateway(true);
		}
	}

	private void linkNodes(int linksCount, Tree tree) {
		for (int i = 0; i < linksCount; i++) {
			int firstNode = inputScanner.nextInt();
			int secondNode = inputScanner.nextInt();
			tree.makeNeighbours(firstNode, secondNode);
		}
	}
		
	private Link unlink(LinkedList<Node> nextPathToUnlink) {
		Node firstNode = nextPathToUnlink.get(0);
		Node secondNode = nextPathToUnlink.get(1);
		firstNode.deleteNeighbour(secondNode);
		secondNode.deleteNeighbour(firstNode);
		LOGGER.info("Next link to cut is {}<->{}",firstNode.getValue(), secondNode.getValue());
		return new Link(firstNode, secondNode);
	}

	
}
