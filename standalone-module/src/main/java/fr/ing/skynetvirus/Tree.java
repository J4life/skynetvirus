package fr.ing.skynetvirus;

public class Tree {
	
	private Node[] nodes;
	private int size;
	private int gatewaysNumber;
	
	public Tree(int size, int gatewaysNumber) {
		super();
		this.setNodes(new Node[size]);
		this.size = size;
		this.gatewaysNumber = gatewaysNumber;
		this.init();
	}
	
	public void init()
	{
		for(int i =0;i<size;i++)
			nodes[i] = new Node(i);
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public int getGatewaysNumber() {
		return gatewaysNumber;
	}
	public void setGatewaysNumber(int gatewaysNumber) {
		this.gatewaysNumber = gatewaysNumber;
	}

	public Node[] getNodes() {
		return nodes;
	}

	public void setNodes(Node[] nodes) {
		this.nodes = nodes;
	}

	public Node getNode(int nodePosition) {
		return nodes[nodePosition];
		
	}

	public void makeNeighbours(int firstNode, int secondNode) {
		getNode(firstNode).addNeighbour(getNode(secondNode));
		getNode(secondNode).addNeighbour(getNode(firstNode));
	}
	
	
	
	
	

}
