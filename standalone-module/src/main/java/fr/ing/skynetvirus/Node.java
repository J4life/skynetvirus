package fr.ing.skynetvirus;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.assertj.core.util.Preconditions;

public class Node {
	
	private int value;
	private List<Node> neighbours;
	//instead of doing inheritance, i prefer to use this boolean to avoid instanceof verifications
	private boolean gateway;
	
	public Node(int value) {
		super();
		this.value = value;
		this.neighbours=new ArrayList<Node>();
		this.gateway = false;
	}
	
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public List<Node> getNeighbours() {
		return neighbours;
	}
	public void setNeighbours(List<Node> neighbours) {
		this.neighbours = neighbours;
	}
	public boolean isGateway() {
		return gateway;
	}
	public void setGateway(boolean gateway) {
		this.gateway = gateway;
	}
	
	public void addNeighbour(Node neighbour)
	{
		this.neighbours.add(neighbour);
	}
	
	public void deleteNeighbour(Node neighbour)
	{
		this.neighbours.remove(neighbour);
	}
	
	@Override
	public boolean equals(Object obj) {
		Preconditions.checkNotNull(obj);
		return ((Node)obj).getValue() == this.getValue();
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
	

}
