package fr.ing.skynetvirus;

import java.util.LinkedList;

public interface ShortestPathStrategy {

	LinkedList<Node> findShortestPath(Tree tree, Node startNode, Node arrivalNode);
}
